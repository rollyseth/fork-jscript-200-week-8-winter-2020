
let myPromise = new Promise(function(resolve, reject) {
  setTimeout(()=> {
    const random =Math.random(); 
    if(random >0.5) {
      
      resolve(random); 

    } else {
      reject('fail'); 
    }

  }, 1000);
});


myPromise.then (number => {
  console.log(`Success`);
  console.log('Complete!');
})
.catch (e => {
  console.log('fail');
  console.log('Complete!');
});