let color=255;
let countDownInterval=setInterval(function () {
 if(color>0) {
     document.querySelector('body').style.backgroundColor=`rgb(${color},${color},${color})`;
     color--;
 }
 else{
     clearInterval(countDownInterval);
 }

}, 500); 