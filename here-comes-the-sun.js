
let color=0

var start = null;

document.querySelector('body').style.backgroundColor= `rgb(0,0,0)`;

function step(timestamp) {
  if (!start) start = timestamp;
  var progress = timestamp - start;
  document.querySelector('body').style.backgroundColor=`rgb(${color},${color},${color})`;
  console.log(`color = ${color}, ${color}, ${color}`);
  color += 1; 
  if (progress < 4000 || color!=255) {
    window.requestAnimationFrame(step);
  }

  if(color===255)
  {
      console.log("Color Animation Complete!")
  }
}

window.requestAnimationFrame(step);



