// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
//const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';
//const BASE_URL='https://api.nytimes.com/svc/books/v3/lists/2019-01-20/hardcover-fiction.json';
//          https://api.nytimes.com/svc/books/v3/lists/2019-2-23/hardcover-fiction.json?api-key=Ne4RCvYsTUG27h59Ob5oO6JPB8kyhnri
//works ... https://api.nytimes.com/svc/books/v3/lists/2019-01-20/hardcover-fiction.json?api-key=Ne4RCvYsTUG27h59Ob5oO6JPB8kyhnri
let BASE_URL= new URL('https://api.nytimes.com/svc/books/v3/lists/');
// const API_KEY='Ne4RCvYsTUG27h59Ob5oO6JPB8kyhnri';
var dd,mm, yy;
var inputdate;
let newURL;
var fetchflag='false'; 
var x; //count variable 
var titlearr, authorarr,descarr; 

//default url
let url=`${BASE_URL}2009-02-21/hardcover-fiction.json?api-key=${API_KEY}`; // = `${BASE_URL}?q=tech&api-key=${API_KEY}`;
const form=document.getElementById('connect-form');

//SUBMIT EVENT LISTENER
form.addEventListener('submit', function(e){
 
  dd=document.getElementsByName('date')[0].value; 
  mm=document.getElementsByName('month')[0].value;
  yy=document.getElementsByName('year')[0].value;

  console.log(`date , month, year: ${dd} ${mm} ${yy}`);
  // const inputs=document.getElementsByClassName('validate-input'); 
  let newURL =`${BASE_URL}${yy}-${mm}-${dd}/hardcover-fiction.json`;
  // let newURL= new URL('https://api.nytimes.com/svc/books/v3/lists/','2009-2-20','/hardcover-fiction.json'); 
   console.log(`Updated URL: ${newURL}`);
   url=`${newURL}?api-key=${API_KEY}`;
   console.log(`final url: ${url}`);
  //fetchflag='true';

e.preventDefault(); 

fetch(url)
.then(function(data) {
 return data.json();
 })
.then(function(responseJson) 
 {
console.log(responseJson);
console.log("fetching done...");
let books = responseJson.results.books;
console.log(books); 

  for (x=0; x<5;x++)
  { 
   titlearr = books[x].title;
   authorarr = books[x].author;
   descarr=books[x].description;
   var divNew= document.createElement("div");
   var number=x+1;
   divNew.innerHTML='['+number+ ']' +' Book Title: \n '+titlearr+ ', Book Author: \n' + authorarr + ', Book Description: \n'+descarr; 
   document.body.appendChild(divNew);
   //const imgURL=`${books[x].book_image}`;
   //console.log(`image url ${imgURL} `);
   var imgEl = new Image(); 
   document.body.appendChild(imgEl);
   imgEl.src = `${books[x].book_image}`;
  }
 });
})