describe('soccer functions', ()=>{
  describe('getTotalPoints',()=>{
    it('Should give me 7 points for "wwdl" ', ()=>{
        const points =getTotalPoints('wwdl');
        expect(points).toBe(7);
    })

    it('Should give me 5 points for "ddddd" ', ()=>{
        const points =getTotalPoints('ddddd');
        expect(points).toBe(5);
    });
}); 

    describe ('orderTeams', ()=>{
        
            const team = {
                name:'Sounders',
                results: 'wdlw'
            };
 
         it('Should output correct team "name: points"',()=>{
            const output =orderTeams(team);
            expect (output).toBe('Sounders: 7');
        });
        
        it('Should output correct team "name: points" with multiple teams',()=>{

            const output =orderTeams(team, team);
            expect (output).toBe('Sounders: 7\nSounders: 7');
        });



    });

  });
